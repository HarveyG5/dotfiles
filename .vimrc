let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'jiangmiao/auto-pairs'
Plug 'lifepillar/vim-mucomplete'
Plug 'lervag/vimtex'
Plug 'tomtom/tcomment_vim'
call plug#end()

"more characters will be sent to the screen for redrawing
set ttyfast
"make backspace behave properly in insert mode
set backspace=indent,eol,start
"a better menu in command mode
set wildmenu
set wildmode=longest:full,full
"hide buffers instead of closing them even if they contain unwritten changes
set hidden
"disable soft wrap for lines
set nowrap
"always display the status line
set laststatus=2
"display line numbers on the left side
set relativenumber
"highlight current line
set cursorline
"display text width column
set colorcolumn=81
"new splits will be at the bottom or to the right side of the screen
set splitbelow
set splitright

set autoindent

"incremental search
set incsearch
"highlight search
set hlsearch
"searches are case insensitive unless they contain at least one capital letter
set ignorecase
set smartcase

set tabstop=4
set shiftwidth=4
set expandtab

"block as cursor
let &t_SI = "\e[6 q"
let &t_SR = "\e[3 q"
let &t_EI = "\e[1 q"
let &t_VS = "\e[1 q"
"fix delay after changing from insert mode to normal mode
set ttimeout
set ttimeoutlen=10

"disable showmode
set noshowmode
"do not display incomplete commands
set noshowcmd
"do not display filename when opening a file
let &shortmess = "F"
"disable swapfiles
set noswapfile

set clipboard=unnamedplus

"completion plugin
set completeopt+=menuone
set completeopt+=noselect
set shortmess+=c   " Shut off completion messages
let g:mucomplete#enable_auto_at_startup = 1

set background=dark
colorscheme gruvbox
let g:airline_theme='base16_gruvbox_dark_hard'
let g:airline_powerline_fonts = 1

"vimtex
syntax enable
let g:vimtex_view_method = 'zathura'
let maplocalleader = ","

hi Normal guibg=NONE ctermbg=NONE
